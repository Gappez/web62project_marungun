using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MarunGun.Data;
using MarunGun.Models;

namespace MarunGun.Pages.RecordAdmin
{
    public class CreateModel : PageModel
    {
        private readonly MarunGun.Data.MarunGunContext _context;

        public CreateModel(MarunGun.Data.MarunGunContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["RunUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public Record Record { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.recordList.Add(Record);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}