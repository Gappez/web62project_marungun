using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MarunGun.Data;
using MarunGun.Models;

namespace MarunGun.Pages.RecordAdmin
{
    public class IndexModel : PageModel
    {
        private readonly MarunGun.Data.MarunGunContext _context;

        public IndexModel(MarunGun.Data.MarunGunContext context)
        {
            _context = context;
        }

        public IList<Record> Record { get;set; }

        public async Task OnGetAsync()
        {
            Record = await _context.recordList
                .Include(r => r.postUser).ToListAsync();
        }
    }
}
