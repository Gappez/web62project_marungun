using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MarunGun.Data;
using MarunGun.Models;

namespace MarunGun.Pages.RecordAdmin
{
    public class EditModel : PageModel
    {
        private readonly MarunGun.Data.MarunGunContext _context;

        public EditModel(MarunGun.Data.MarunGunContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Record Record { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Record = await _context.recordList
                .Include(r => r.postUser).FirstOrDefaultAsync(m => m.RecordID == id);

            if (Record == null)
            {
                return NotFound();
            }
           ViewData["RunUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Record).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecordExists(Record.RecordID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool RecordExists(int id)
        {
            return _context.recordList.Any(e => e.RecordID == id);
        }
    }
}
