using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MarunGun.Models;
using MarunGun.Data;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace MarunGun.Pages
{
    public class IndexModel : PageModel
    {
        private readonly MarunGun.Data.MarunGunContext _context;

        public IndexModel(MarunGun.Data.MarunGunContext context)
        {
            _context = context;
        }
        public IList<Record> Record { get; set; }
        public IList<MarunGunMeet> Meet{get; set;}
        public async Task OnGetAsync()
        {
            Record = await _context.recordList
                .Include(p=>p.postUser).ToListAsync();
            Meet = await _context.meetRun    
                   .Include(p=>p.postMeet).ToListAsync();
            


        }
        
        


    }
}
