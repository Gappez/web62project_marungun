using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MarunGun.Data;
using MarunGun.Models;

namespace MarunGun.Pages.MeetAdmin
{
    public class EditModel : PageModel
    {
        private readonly MarunGun.Data.MarunGunContext _context;

        public EditModel(MarunGun.Data.MarunGunContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MarunGunMeet MarunGunMeet { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MarunGunMeet = await _context.meetRun
                .Include(m => m.postMeet).FirstOrDefaultAsync(m => m.MarunGunMeetID == id);

            if (MarunGunMeet == null)
            {
                return NotFound();
            }
           ViewData["RunUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(MarunGunMeet).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MarunGunMeetExists(MarunGunMeet.MarunGunMeetID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MarunGunMeetExists(int id)
        {
            return _context.meetRun.Any(e => e.MarunGunMeetID == id);
        }
    }
}
