using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MarunGun.Data;
using MarunGun.Models;

namespace MarunGun.Pages.MeetAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly MarunGun.Data.MarunGunContext _context;

        public DeleteModel(MarunGun.Data.MarunGunContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MarunGunMeet MarunGunMeet { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MarunGunMeet = await _context.meetRun
                .Include(m => m.postMeet).FirstOrDefaultAsync(m => m.MarunGunMeetID == id);

            if (MarunGunMeet == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MarunGunMeet = await _context.meetRun.FindAsync(id);

            if (MarunGunMeet != null)
            {
                _context.meetRun.Remove(MarunGunMeet);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
