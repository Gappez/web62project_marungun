using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MarunGun.Data;
using MarunGun.Models;

namespace MarunGun.Pages.MeetAdmin
{
    public class IndexModel : PageModel
    {
        private readonly MarunGun.Data.MarunGunContext _context;

        public IndexModel(MarunGun.Data.MarunGunContext context)
        {
            _context = context;
        }

        public IList<MarunGunMeet> MarunGunMeet { get;set; }

        public async Task OnGetAsync()
        {
            MarunGunMeet = await _context.meetRun
                .Include(m => m.postMeet).ToListAsync();
        }
    }
}
