using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MarunGun.Pages
{
    public class EventModel : PageModel
    {
        public string Message { get; set; }

        public void OnGet()
        {
            Message = "กิจกรรมงานวิ่ง";
        }
    }
}
