﻿using MarunGun.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace MarunGun.Data
{
	public class MarunGunContext : IdentityDbContext<RunUser>
	{
		public DbSet<Record> recordList { get; set; }
		public DbSet<MarunGunMeet> meetRun{get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
			optionsBuilder.UseSqlite(@"Data source=RecordRun.db");
        }
	}
}
