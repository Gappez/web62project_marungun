﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
namespace MarunGun.Models
{

	public class RunUser : IdentityUser
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public double Goalperday{ get; set;}
		public double Goal{get; set;}
	}
	public class Record
	{
		public int RecordID { get; set; }
		public string Location { get; set; }
		[DataType(DataType.DateTime)]
		public DateTime DateRun { get; set; }
		[DataType(DataType.Time)]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:H:mm}")]
		public DateTime TimeRun { get; set; }
		public double TimeRunning{get; set;}
		public double Distance { get; set; }
		public string RunUserId { get; set; }
		public RunUser postUser { get; set; }
	}
	public class MarunGunMeet{
		public int MarunGunMeetID{get; set;}
		public string RunUserId{get; set;}
		public RunUser postMeet{get; set;}
		public string Meeting{get; set;}
		[DataType(DataType.Date)]
		public string DateMeet { get; set; }
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:H:mm}")]
		public DateTime TimeMeet { get; set; }
	}
}
